<?php

$domdoc = new DOMDocument();
$domdoc->load("SkierLogs.xml");

$skier = $domdoc->getElementsByTagName('Skier');
$club = $domdoc->getElementsByTagName('Club');
$season = $domdoc->getElementsByTagName('Season');


try {
  $connection = new PDO('mysql:host=localhost;dbname=assignment5', 'root', '');
  $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  foreach ($skier as $skier) {
    if ($skier->parentNode->parentNode->nodeName == "SkierLogs") {
      $userName = $skier->getAttribute('userName');
      $firstName = $skier->firstChild->nextSibling;
      $lastName = $firstName->nextSibling->nextSibling;
      $dateOfBirth = $lastName->nextSibling->nextSibling;

      $sql = "INSERT INTO skiers (userName, firstName, lastName, yearOfBirth)
              VALUES ('$userName', '$firstName->nodeValue', '$lastName->nodeValue', $dateOfBirth->nodeValue)";

      $connection->exec($sql);
    }
  }
  echo "Added all skiers to database.<br>";

  foreach ($club as $club) {
    $clubId = $club->getAttribute('id');
    $name = $club->firstChild->nextSibling;
    $city = $name->nextSibling->nextSibling;
    $county = $city->nextSibling->nextSibling;

    $sql = "INSERT INTO clubs (clubId, name, city, county)
            VALUES ('$clubId', '$name->nodeValue', '$city->nodeValue', '$county->nodeValue')";

    $connection->exec($sql);
  }
  echo "Added all clubs to database.<br>";

  foreach ($season as $season) {
    $fallYear = $season->getAttribute('fallYear');
    foreach ($season->getElementsByTagName('Skier') as $skier) {
      $totalDistance = 0;
      $club = $skier->parentNode->getAttribute('clubId');
      $userName = $skier->getAttribute('userName');

      foreach ($skier->getElementsByTagName('Distance') as $distance) {
        $totalDistance += $distance->nodeValue;
      }
      if ($club == NULL) {
        $sql = "INSERT INTO season (fallYear, userName, totalDistance)
                VALUES ('$fallYear', '$userName', '$totalDistance')";
      }
      else {
        $sql = "INSERT INTO season (fallYear, clubId, userName, totalDistance)
                VALUES ('$fallYear', '$club', '$userName', '$totalDistance')";
      }

      $connection->exec($sql);

    }
    echo "Added data from " . $fallYear . "<br>";

  }
}

catch (PDOException $e) {
  echo $sql . "<br>" . $e->getMessage();
}

$connection = null;

?>
