-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 18:53 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubs`
--

CREATE TABLE `clubs` (
  `clubId` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `county` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `clubs`
--

INSERT INTO `clubs` (`clubId`, `name`, `city`, `county`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim', 'SÃ¸r-TrÃ¸ndelag'),
('vindil', 'Vind Idrettslag', 'GjÃ¸vik', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` int(11) NOT NULL,
  `clubId` varchar(64) DEFAULT NULL,
  `userName` varchar(64) NOT NULL,
  `totalDistance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `season`
--

INSERT INTO `season` (`fallYear`, `clubId`, `userName`, `totalDistance`) VALUES
(2015, 'skiklubben', 'ande_andr', 23),
(2015, 'lhmr-ski', 'ande_rÃ¸nn', 942),
(2015, 'asker-ski', 'andr_stee', 440),
(2015, 'skiklubben', 'anna_nÃ¦ss', 3),
(2015, 'skiklubben', 'arne_anto', 32),
(2015, 'skiklubben', 'arne_inge', 1),
(2015, 'lhmr-ski', 'astr_amun', 961),
(2015, 'skiklubben', 'astr_sven', 1),
(2015, 'skiklubben', 'Ã¸yst_aase', 2),
(2015, 'skiklubben', 'Ã¸yst_lore', 13),
(2015, 'vindil', 'Ã¸yst_sÃ¦th', 831),
(2015, 'asker-ski', 'Ã¸yvi_hell', 950),
(2015, 'skiklubben', 'Ã¸yvi_jens', 3),
(2015, 'asker-ski', 'Ã¸yvi_kvam', 18),
(2015, 'asker-ski', 'Ã¸yvi_vike', 20),
(2015, 'asker-ski', 'bent_hÃ¥la', 19),
(2015, 'asker-ski', 'bent_svee', 125),
(2015, 'asker-ski', 'beri_hans', 448),
(2015, 'asker-ski', 'bjÃ¸r_aase', 121),
(2015, 'asker-ski', 'bjÃ¸r_ali', 47),
(2015, 'lhmr-ski', 'bjÃ¸r_rÃ¸nn', 33),
(2015, 'lhmr-ski', 'bjÃ¸r_sand', 460),
(2015, 'vindil', 'cami_erik', 1),
(2015, 'lhmr-ski', 'dani_hamm', 33),
(2015, 'lhmr-ski', 'eina_nygÃ¥', 31),
(2015, 'asker-ski', 'elis_ruud', 341),
(2015, 'lhmr-ski', 'elle_wiik', 12),
(2015, 'lhmr-ski', 'erik_haal', 122),
(2015, 'vindil', 'erik_lien', 1),
(2015, 'vindil', 'erik_pete', 581),
(2015, 'skiklubben', 'espe_egel', 519),
(2015, 'lhmr-ski', 'espe_haal', 1),
(2015, 'skiklubben', 'eva_kvam', 28),
(2015, 'asker-ski', 'fred_lien', 113),
(2015, 'skiklubben', 'frod_mads', 1),
(2015, 'lhmr-ski', 'frod_rÃ¸nn', 237),
(2015, 'skiklubben', 'geir_birk', 69),
(2015, 'asker-ski', 'geir_herm', 891),
(2015, 'lhmr-ski', 'gerd_svee', 173),
(2015, 'vindil', 'gunn_berg', 2),
(2015, 'asker-ski', 'hans_foss', 240),
(2015, 'skiklubben', 'hans_lÃ¸ke', 3),
(2015, 'lhmr-ski', 'hara_bakk', 7),
(2015, 'lhmr-ski', 'hÃ¥ko_jens', 778),
(2015, 'asker-ski', 'heid_dani', 3),
(2015, 'skiklubben', 'helg_brei', 27),
(2015, 'skiklubben', 'helg_toll', 9),
(2015, 'asker-ski', 'henr_bern', 799),
(2015, NULL, 'henr_dale', 2),
(2015, 'vindil', 'henr_lore', 1),
(2015, 'lhmr-ski', 'hild_hass', 2),
(2015, 'skiklubben', 'ida_mykl', 666),
(2015, 'asker-ski', 'inge_simo', 3),
(2015, NULL, 'inge_thor', 194),
(2015, 'skiklubben', 'ingr_edva', 294),
(2015, NULL, 'ï»¿jan_tang', 2),
(2015, 'skiklubben', 'juli_ande', 20),
(2015, 'skiklubben', 'kari_thor', 261),
(2015, 'asker-ski', 'kjel_fjel', 1),
(2015, 'lhmr-ski', 'knut_bye', 2),
(2015, 'skiklubben', 'kris_even', 586),
(2015, 'skiklubben', 'kris_hass', 4),
(2015, 'skiklubben', 'kris_hass1', 391),
(2015, 'lhmr-ski', 'lind_lore', 578),
(2015, 'asker-ski', 'liv_khan', 178),
(2015, 'asker-ski', 'magn_sand', 200),
(2015, NULL, 'mari_bye', 362),
(2015, 'lhmr-ski', 'mari_dahl', 576),
(2015, 'lhmr-ski', 'mari_eile', 18),
(2015, 'skiklubben', 'mari_stra', 41),
(2015, 'vindil', 'mart_halv', 63),
(2015, 'skiklubben', 'mona_lie', 7),
(2015, 'skiklubben', 'mort_iver', 2),
(2015, 'lhmr-ski', 'nils_bakk', 36),
(2015, 'skiklubben', 'nils_knud', 4),
(2015, 'skiklubben', 'odd_moha', 352),
(2015, NULL, 'olav_brÃ¥t', 17),
(2015, 'lhmr-ski', 'olav_eike', 2),
(2015, 'skiklubben', 'olav_hell', 1),
(2015, 'asker-ski', 'olav_lien', 408),
(2015, 'lhmr-ski', 'ole_borg', 311),
(2015, 'skiklubben', 'reid_hamr', 2),
(2015, 'skiklubben', 'rolf_wiik', 749),
(2015, 'asker-ski', 'rune_haga', 228),
(2015, 'asker-ski', 'silj_mykl', 1),
(2015, NULL, 'solv_solb', 2),
(2015, NULL, 'stia_andr', 8),
(2015, 'skiklubben', 'stia_haug', 412),
(2015, 'vindil', 'stia_henr', 62),
(2015, 'skiklubben', 'terj_mort', 119),
(2015, 'vindil', 'thom_inge', 15),
(2015, 'vindil', 'tom_bÃ¸e', 176),
(2015, 'vindil', 'tom_brÃ¥t', 1),
(2015, 'asker-ski', 'tom_jako', 18),
(2015, 'lhmr-ski', 'tore_gulb', 375),
(2015, 'skiklubben', 'tore_svee', 1156),
(2015, 'skiklubben', 'tor_dale', 408),
(2015, 'asker-ski', 'tove_moe', 321),
(2015, 'skiklubben', 'tron_kris', 3),
(2015, 'vindil', 'tron_moen', 8),
(2016, 'skiklubben', 'ande_andr', 55),
(2016, 'asker-ski', 'andr_stee', 379),
(2016, 'skiklubben', 'anna_nÃ¦ss', 3),
(2016, 'skiklubben', 'arne_anto', 99),
(2016, 'skiklubben', 'arne_inge', 2),
(2016, 'lhmr-ski', 'astr_amun', 761),
(2016, 'skiklubben', 'astr_sven', 3),
(2016, 'skiklubben', 'Ã¸yst_aase', 1),
(2016, 'skiklubben', 'Ã¸yst_lore', 47),
(2016, 'vindil', 'Ã¸yst_sÃ¦th', 631),
(2016, 'asker-ski', 'Ã¸yvi_hell', 869),
(2016, 'skiklubben', 'Ã¸yvi_jens', 2),
(2016, 'asker-ski', 'Ã¸yvi_vike', 52),
(2016, 'skiklubben', 'bent_hÃ¥la', 62),
(2016, 'asker-ski', 'beri_hans', 374),
(2016, 'asker-ski', 'bjÃ¸r_aase', 116),
(2016, 'asker-ski', 'bjÃ¸r_ali', 47),
(2016, 'lhmr-ski', 'bjÃ¸r_rÃ¸nn', 56),
(2016, 'lhmr-ski', 'bjÃ¸r_sand', 449),
(2016, 'skiklubben', 'bror_ï»¿mos', 243),
(2016, NULL, 'bror_kals', 202),
(2016, 'vindil', 'cami_erik', 1),
(2016, 'lhmr-ski', 'dani_hamm', 61),
(2016, 'skiklubben', 'eina_nygÃ¥', 68),
(2016, 'skiklubben', 'elis_ruud', 368),
(2016, 'lhmr-ski', 'elle_wiik', 35),
(2016, 'lhmr-ski', 'erik_haal', 143),
(2016, 'skiklubben', 'espe_egel', 556),
(2016, 'lhmr-ski', 'espe_haal', 2),
(2016, 'skiklubben', 'eva_kvam', 89),
(2016, 'asker-ski', 'fred_lien', 122),
(2016, 'skiklubben', 'frod_mads', 2),
(2016, 'skiklubben', 'geir_birk', 71),
(2016, 'skiklubben', 'geir_herm', 789),
(2016, 'lhmr-ski', 'gerd_svee', 196),
(2016, 'vindil', 'gunn_berg', 2),
(2016, 'skiklubben', 'guri_nord', 17),
(2016, 'lhmr-ski', 'hann_stei', 14),
(2016, 'lhmr-ski', 'hans_foss', 276),
(2016, 'skiklubben', 'hans_lÃ¸ke', 1),
(2016, 'lhmr-ski', 'hara_bakk', 16),
(2016, 'lhmr-ski', 'hÃ¥ko_jens', 804),
(2016, 'asker-ski', 'heid_dani', 3),
(2016, 'skiklubben', 'helg_brei', 74),
(2016, NULL, 'henr_dale', 2),
(2016, 'vindil', 'henr_lore', 1),
(2016, 'lhmr-ski', 'hild_hass', 1),
(2016, 'skiklubben', 'idar_kals', 101),
(2016, 'vindil', 'idar_kals1', 1308),
(2016, 'skiklubben', 'ida_mykl', 614),
(2016, 'asker-ski', 'inge_simo', 2),
(2016, NULL, 'inge_thor', 220),
(2016, 'skiklubben', 'ingr_edva', 309),
(2016, 'asker-ski', 'ï»¿hal_ï»¿mos', 3),
(2016, NULL, 'ï»¿jan_tang', 4),
(2016, 'vindil', 'ï»¿rut_ï»¿mos', 1237),
(2016, 'skiklubben', 'ï»¿rut_nord', 368),
(2016, 'skiklubben', 'juli_ande', 34),
(2016, 'skiklubben', 'kari_thor', 233),
(2016, 'skiklubben', 'kjel_fjel', 2),
(2016, 'skiklubben', 'kris_hass', 11),
(2016, 'lhmr-ski', 'kris_hass1', 334),
(2016, 'lhmr-ski', 'lind_lore', 551),
(2016, 'asker-ski', 'liv_khan', 183),
(2016, 'asker-ski', 'magn_sand', 166),
(2016, 'lhmr-ski', 'mari_dahl', 492),
(2016, 'lhmr-ski', 'mari_eile', 18),
(2016, 'skiklubben', 'mari_stra', 35),
(2016, 'vindil', 'mart_halv', 50),
(2016, 'skiklubben', 'mona_lie', 12),
(2016, 'skiklubben', 'mort_iver', 4),
(2016, 'lhmr-ski', 'nils_bakk', 93),
(2016, 'skiklubben', 'nils_knud', 2),
(2016, NULL, 'olav_brÃ¥t', 19),
(2016, 'lhmr-ski', 'olav_eike', 2),
(2016, 'asker-ski', 'olav_lien', 423),
(2016, 'lhmr-ski', 'ole_borg', 314),
(2016, 'skiklubben', 'reid_hamr', 3),
(2016, 'skiklubben', 'rolf_wiik', 632),
(2016, 'asker-ski', 'rune_haga', 248),
(2016, 'asker-ski', 'sara_okst', 5),
(2016, 'asker-ski', 'silj_mykl', 2),
(2016, 'skiklubben', 'sive_nord', 1),
(2016, 'asker-ski', 'solv_solb', 1),
(2016, 'vindil', 'stia_andr', 9),
(2016, 'skiklubben', 'stia_haug', 443),
(2016, 'vindil', 'stia_henr', 49),
(2016, 'skiklubben', 'terj_mort', 95),
(2016, 'vindil', 'thom_inge', 26),
(2016, 'vindil', 'tom_bÃ¸e', 194),
(2016, 'vindil', 'tom_brÃ¥t', 1),
(2016, 'skiklubben', 'tom_jako', 33),
(2016, 'lhmr-ski', 'tore_gulb', 342),
(2016, 'asker-ski', 'tove_moe', 352),
(2016, 'lhmr-ski', 'trin_kals', 22),
(2016, 'skiklubben', 'tron_kris', 5),
(2016, 'vindil', 'tron_moen', 17);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skiers`
--

CREATE TABLE `skiers` (
  `userName` char(64) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `yearOfBirth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skiers`
--

INSERT INTO `skiers` (`userName`, `firstName`, `lastName`, `yearOfBirth`) VALUES
('ande_andr', 'Anders', 'Andresen', 2004),
('ande_rÃ¸nn', 'Anders', 'RÃ¸nning', 2001),
('andr_stee', 'Andreas', 'Steen', 2001),
('anna_nÃ¦ss', 'Anna', 'NÃ¦ss', 2005),
('arne_anto', 'Arne', 'Antonsen', 2005),
('arne_inge', 'Arne', 'Ingebrigtsen', 2005),
('astr_amun', 'Astrid', 'Amundsen', 2001),
('astr_sven', 'Astrid', 'Svendsen', 2008),
('Ã¸yst_aase', 'Ã˜ystein', 'Aasen', 2007),
('Ã¸yst_lore', 'Ã˜ystein', 'Lorentzen', 2004),
('Ã¸yst_sÃ¦th', 'Ã˜ystein', 'SÃ¦ther', 2000),
('Ã¸yvi_hell', 'Ã˜yvind', 'Helle', 2000),
('Ã¸yvi_jens', 'Ã˜yvind', 'Jenssen', 1999),
('Ã¸yvi_kvam', 'Ã˜yvind', 'Kvam', 2000),
('Ã¸yvi_vike', 'Ã˜yvind', 'Viken', 2004),
('bent_hÃ¥la', 'Bente', 'HÃ¥land', 2009),
('bent_svee', 'Bente', 'Sveen', 2003),
('beri_hans', 'Berit', 'Hanssen', 2003),
('bjÃ¸r_aase', 'BjÃ¸rn', 'Aasen', 2006),
('bjÃ¸r_ali', 'BjÃ¸rn', 'Ali', 2008),
('bjÃ¸r_rÃ¸nn', 'BjÃ¸rg', 'RÃ¸nningen', 2009),
('bjÃ¸r_sand', 'BjÃ¸rn', 'Sandvik', 1997),
('bror_ï»¿mos', 'Bror', 'ï»¿Mostuen', 2005),
('bror_kals', 'Bror', 'Kalstad', 2006),
('cami_erik', 'Camilla', 'Eriksen', 2005),
('dani_hamm', 'Daniel', 'Hammer', 2000),
('eina_nygÃ¥', 'Einar', 'NygÃ¥rd', 2009),
('elis_ruud', 'Elisabeth', 'Ruud', 2003),
('elle_wiik', 'Ellen', 'Wiik', 2004),
('erik_haal', 'Erik', 'Haaland', 2007),
('erik_lien', 'Erik', 'Lien', 2008),
('erik_pete', 'Erik', 'Petersen', 2002),
('espe_egel', 'Espen', 'Egeland', 2005),
('espe_haal', 'Espen', 'Haaland', 2004),
('eva_kvam', 'Eva', 'Kvam', 2000),
('fred_lien', 'Fredrik', 'Lien', 2000),
('frod_mads', 'Frode', 'Madsen', 2008),
('frod_rÃ¸nn', 'Frode', 'RÃ¸nningen', 2005),
('geir_birk', 'Geir', 'Birkeland', 2010),
('geir_herm', 'Geir', 'Hermansen', 2003),
('gerd_svee', 'Gerd', 'Sveen', 2001),
('gunn_berg', 'Gunnar', 'Berge', 2009),
('guri_nord', 'Guri', 'Nordli', 2003),
('hann_stei', 'Hanno', 'Steiro', 2005),
('hans_foss', 'Hans', 'Foss', 1998),
('hans_lÃ¸ke', 'Hans', 'LÃ¸ken', 2005),
('hara_bakk', 'Harald', 'Bakken', 2002),
('hÃ¥ko_jens', 'HÃ¥kon', 'Jensen', 2005),
('heid_dani', 'Heidi', 'Danielsen', 2005),
('helg_brei', 'Helge', 'Breivik', 2006),
('helg_toll', 'Helge', 'Tollefsen', 2003),
('henr_bern', 'Henrik', 'Berntsen', 2003),
('henr_dale', 'Henrik', 'Dalen', 2005),
('henr_lore', 'Henrik', 'Lorentzen', 2006),
('hild_hass', 'Hilde', 'Hassan', 2007),
('idar_kals', 'Idar', 'Kalstad', 2007),
('idar_kals1', 'Idar', 'Kalstad', 2002),
('ida_mykl', 'Ida', 'Myklebust', 2001),
('inge_simo', 'Inger', 'Simonsen', 2004),
('inge_thor', 'Inger', 'Thorsen', 2006),
('ingr_edva', 'Ingrid', 'Edvardsen', 2001),
('ï»¿hal_ï»¿mos', 'ï»¿Halvor', 'ï»¿Mostuen', 2009),
('ï»¿jan_tang', 'ï»¿Jan', 'Tangen', 2007),
('ï»¿rut_ï»¿mos', 'ï»¿Ruth', 'ï»¿Mostuen', 2002),
('ï»¿rut_nord', 'ï»¿Ruth', 'Nordli', 2006),
('juli_ande', 'Julie', 'Andersson', 2003),
('kari_thor', 'Karin', 'Thorsen', 2002),
('kjel_fjel', 'Kjell', 'Fjeld', 2004),
('knut_bye', 'Knut', 'Bye', 2006),
('kris_even', 'Kristian', 'Evensen', 2004),
('kris_hass', 'Kristin', 'Hassan', 2003),
('kris_hass1', 'Kristian', 'Hassan', 2004),
('lind_lore', 'Linda', 'Lorentzen', 2004),
('liv_khan', 'Liv', 'Khan', 2006),
('magn_sand', 'Magnus', 'Sande', 2003),
('mari_bye', 'Marit', 'Bye', 2003),
('mari_dahl', 'Marit', 'Dahl', 2004),
('mari_eile', 'Marius', 'Eilertsen', 2000),
('mari_stra', 'Marius', 'Strand', 2005),
('mart_halv', 'Martin', 'Halvorsen', 2002),
('mona_lie', 'Mona', 'Lie', 2004),
('mort_iver', 'Morten', 'Iversen', 2003),
('nils_bakk', 'Nils', 'Bakke', 2003),
('nils_knud', 'Nils', 'Knudsen', 2006),
('odd_moha', 'Odd', 'Mohamed', 2005),
('olav_brÃ¥t', 'Olav', 'BrÃ¥then', 2000),
('olav_eike', 'Olav', 'Eikeland', 2008),
('olav_hell', 'Olav', 'Helle', 2007),
('olav_lien', 'Olav', 'Lien', 2002),
('ole_borg', 'Ole', 'Borge', 2002),
('reid_hamr', 'Reidun', 'Hamre', 2008),
('rolf_wiik', 'Rolf', 'Wiik', 2002),
('rune_haga', 'Rune', 'Haga', 2005),
('sara_okst', 'Sarah', 'Okstad', 2003),
('silj_mykl', 'Silje', 'Myklebust', 2007),
('sive_nord', 'Sivert', 'Nordli', 2009),
('solv_solb', 'Solveig', 'Solbakken', 2004),
('stia_andr', 'Stian', 'Andreassen', 2004),
('stia_haug', 'Stian', 'Haugland', 2002),
('stia_henr', 'Stian', 'Henriksen', 2001),
('terj_mort', 'Terje', 'Mortensen', 2003),
('thom_inge', 'Thomas', 'Ingebrigtsen', 2006),
('tom_bÃ¸e', 'Tom', 'BÃ¸e', 2008),
('tom_brÃ¥t', 'Tom', 'BrÃ¥then', 2008),
('tom_jako', 'Tom', 'Jakobsen', 2002),
('tore_gulb', 'Tore', 'Gulbrandsen', 2005),
('tore_svee', 'Tore', 'Sveen', 2001),
('tor_dale', 'Tor', 'Dalen', 2005),
('tove_moe', 'Tove', 'Moe', 2002),
('trin_kals', 'Trine', 'Kalstad', 2009),
('tron_kris', 'Trond', 'Kristensen', 2006),
('tron_moen', 'Trond', 'Moen', 2004);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`,`userName`),
  ADD KEY `clubId` (`clubId`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `season`
--
ALTER TABLE `season`
  ADD CONSTRAINT `season_ibfk_1` FOREIGN KEY (`clubId`) REFERENCES `clubs` (`clubId`),
  ADD CONSTRAINT `season_ibfk_2` FOREIGN KEY (`userName`) REFERENCES `skiers` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
